package week3.assignment_package.dependent;

import week3.assignment_package.independent.Address;

public class Student {
    int roll_no;
    String name;
    Address address;

    public Student(int roll_no, String name, Address address) {
        this.roll_no = roll_no;
        this.name = name;
        this.address = address;
    }


    public String display() {
        return "Student{" +
                "roll_no=" + roll_no +
                ", name='" + name + '\'' +
                ", address=" + address +
                '}';
    }

    public static void main(String[] args) {

        Student student=new Student(111,"praveer",
                new Address(202,"no idea","barotenagar","Kharadi",411140));
        System.out.println(student.display());
        /* we can't access the methode of address class if the methode is private ,default or protected
        we can only access the methode if its public

         */
        Address address1=new Address(202,"abc","nagar","kharadi",411014);
        address1.modifier();
        Student student1=new Student(111,"praveer",address1);
        student1.display();
    }
}
