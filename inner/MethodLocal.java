package week3.assignment_package.inner;



class Outer
{
    void method()
    {
        class Methodlocal
        {
            void insideMethod()
            {
                System.out.println(" method of the inner class");
            }
        }

        Methodlocal methodlocal=new Methodlocal();
        methodlocal.insideMethod();
    }
}
public class MethodLocal {

    public static void main(String[] args) {
        Outer outer=new Outer();
//        outer.innermethod(); we cant directly
        outer.method();
    }


}
