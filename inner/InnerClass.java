package week3.assignment_package.inner;

public class InnerClass {

    int a = 4;

    class Inner {
        void innerDisplay() {
            System.out.println("INNER DISPLAY"+a);

        }
    }


    public static void main(String[] args) {

        InnerClass.Inner i1 = new InnerClass().new Inner();
        i1.innerDisplay();

    }
}
