package week3.assignment_package.inner;

public class StaticInnerClass {

        static class statClass{
            static void display() {
                System.out.println("In the static inner class static method");
            }

            void display2()
            {
                System.out.println("In the static inner class instance method");
            }
        }

        public static void main(String[] args) {

            StaticInnerClass.statClass.display();
            statClass sc=new statClass();
            sc.display2();
        }
    }


