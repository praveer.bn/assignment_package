package week3.assignment_package.inner;


    class Basic{
        public void show()
        {

        }
        public void out()
        {
            System.out.println("Basic");
        }
    }
    public class AnonymousClass {
        public static void main(String[] args) {
            Basic b=new Basic(){
                public void show(){
                    System.out.println("overidden show");
                }
                public void out()
                {
                    System.out.println("overidden out");
                }

                public void display()
                {
                    System.out.println("new display");
                }
            };

            //b.display();      we can't accesss it
            b.out();
            b.show();
        }
    }
/* out put:
overidden out
overidden show

 */

