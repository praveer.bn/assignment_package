package week3.assignment_package.independent;
// independent package
public class Address {
int room_no;
String Flatname;
String Street;
String City;
int Pincode;

    public Address(int room_no, String flatname, String street, String city, int pincode) {
        this.room_no = room_no;
        Flatname = flatname;
        Street = street;
        City = city;
        Pincode = pincode;
    }
    public void modifier(){
        System.out.println("modifier");
    }

    @Override
    public String toString() {
        return "Address{" +
                "room_no=" + room_no +
                ", Flatname='" + Flatname + '\'' +
                ", Street='" + Street + '\'' +
                ", City='" + City + '\'' +
                ", Pincode=" + Pincode +
                '}';
    }
}
